# Gltest

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 15.0.4.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.

## Permisos

sudo chmod -R 777 gltest

## Git global setup

git config --global user.name "Luis Paúl Pinto"
git config --global user.email "paulpinto@live.com"

## Create a new repository

git clone https://gitlab.com/lpaulpinto/xxx.git
cd xxx
git switch -c main
touch README.md
git add README.md
git commit -m "add README"
git push -u origin main

## Push an existing folder

cd existing_folder
git init --initial-branch=main
git remote add origin https://gitlab.com/lpaulpinto/xxx.git
git add .
git commit -m "Initial commit"
git push -u origin main

## Push an existing Git repository

cd existing_repo
git remote rename origin old-origin
git remote add origin https://gitlab.com/lpaulpinto/xxx.git
git push -u origin --all
git push -u origin --tags
